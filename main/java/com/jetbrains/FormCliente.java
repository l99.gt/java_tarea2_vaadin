package com.jetbrains;

import clases.Cliente;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import static com.vaadin.server.Page.getCurrent;

/**
 * Created by L99 on 7/13/2017.
 */
public class FormCliente extends FormLayout {
    private TextField cui = new TextField("Cui");
    private TextField nombre = new TextField("Nombre");
    private TextField telefono = new TextField("Telefono");
    private TextField edad = new TextField("Edad");
    private TextField nit = new TextField("Nit");
    private Button btnSaveCliente = new Button("Guardar Cliente");
    private Label titleCliente = new Label();

    private Cliente clinete;
    private MyUI myUI;

    public FormCliente(MyUI myUI){
        this.myUI = myUI;
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(btnSaveCliente);
        buttons.setSpacing(true);
        btnSaveCliente.setStyleName(ValoTheme.BUTTON_PRIMARY);
        titleCliente.setValue("Ingresar Cliente");
        addComponent(titleCliente);
        addComponents(cui,nombre,telefono,edad,nit,buttons);
        btnSaveCliente.addClickListener(e -> saveCliente());
    }

    public void saveCliente(){
        Cliente clienteSistema = new Cliente(Integer.parseInt(cui.getValue()), nombre.getValue(), telefono.getValue(), Integer.parseInt(edad.getValue()), Integer.parseInt(nit.getValue()));
        myUI.mySistema.setClienteSistema(clienteSistema);
        cui.setValue("");
        nombre.setValue("");
        telefono.setValue("");
        edad.setValue("");
        nit.setValue("");
        Notification notif = new Notification(
                "Datos de Cliente",
                clienteSistema.getNombre()+" Ingresado Correctamente",
                Notification.Type.HUMANIZED_MESSAGE);
        notif.setDelayMsec(10000);
        notif.show(Page.getCurrent());
        this.setVisible(false);
    }






}
