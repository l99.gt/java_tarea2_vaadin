package clases;

import javax.swing.*;

/**
 * Created by L99 on 7/13/2017.
 */
public class Desarrollador extends Persona{
    private int codigo;
    private double sueldo;
    String profesion;

    public Desarrollador(){

    }
    //Se crea el constructor donde es necesario iniciarlizar los atributos de la clase Padre
    //Utilizo "super" para iniciarlizar los atributos heredados.
    public Desarrollador(int cui, String nombre, String telefono, int edad, int codigo, double sueldo, String profesion) {
        super(cui, nombre, telefono, edad);
        this.codigo = codigo;
        this.sueldo = sueldo;
        this.profesion = profesion;
    }

    //Metodos Get y Set para los atributos sueldo y profesion
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public void mostrarDesarrolladores() {
        JOptionPane.showMessageDialog(null, "Cui: "+getCui()
                +"\nNombre: " +getNombre()
                +"\nTelefono: "+getTelefono()
                +"\nEdad: "+getEdad()
                +"\nCodigo: "+getCodigo()
                +"\nSueldo: "+getSueldo()
                +"\nProfesion: "+getProfesion(),"Datos del Desarrollador",1);
    }
}
