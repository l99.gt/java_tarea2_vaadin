package clases;

/**
 * Created by L99 on 7/13/2017.
 */
public class Requerimiento {
    private String descripcion;

    //Se declara constructor vacío
    public Requerimiento() {
    }

    //Se declara constructor con las 2 variables.
    public Requerimiento(String descripcion) {
        this.descripcion = descripcion;
    }

    //Metodos Get y Set para nuestras variables
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
