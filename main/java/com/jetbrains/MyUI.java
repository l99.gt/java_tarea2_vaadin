package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import clases.Cliente;
import clases.Desarrollador;
import clases.Requerimiento;
import clases.Sistema;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
    Sistema mySistema = null;
    private FormCliente form = new FormCliente(this);
    private FormDesarrollador formDesarrollador = new FormDesarrollador(this);
    private FormRequerimiento formRequerimiento = new FormRequerimiento(this);

    private Grid<Requerimiento> gridRF = new Grid<>("Requerimientos Funcionales");
    private Grid<Requerimiento> gridNF = new Grid<>("Requerimientos No Funcionales");
    private Grid<Desarrollador> gridDes = new Grid<>("Desarrolladores");






    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layoutV = new VerticalLayout();
        final HorizontalLayout layoutH = new HorizontalLayout();




        //Datos Sistemas
        final TextField cantDes = new TextField();
        cantDes.setCaption("Cant. de Desarrolladores:");
        final TextField nomSis = new TextField();
        nomSis.setCaption("Nombre del Sistema:");
        Button btnAddSis = new Button("Agregar Datos del Sistema");
        btnAddSis.setStyleName(ValoTheme.BUTTON_PRIMARY);
        Button btnMostrarDatosSis = new Button("Mostrar Datos");

        //menu
        MenuBar barmenu = new MenuBar();
        MenuBar.MenuItem bmSis  = barmenu.addItem("Nuevo Sistema",null,null);
        // Submenu item with a sub-submenu
        MenuBar.MenuItem bmDatosSis = bmSis.addItem("Datos Sistema", null, null);
        bmDatosSis.addItem("Agregar Datos", e ->  {
            cantDes.setVisible(true);
            nomSis.setVisible(true);
            btnAddSis.setVisible(true);
            layoutH.addComponents(cantDes, nomSis,btnAddSis);});
        bmDatosSis.addItem("Mostrar Datos",e -> {
            Notification notif = new Notification(
                    "Datos de Sistemas",
                    "Cant. Desarrolladores: "
                    + mySistema.cantDesarrolladores() +"\n\n\nNombre Sistema: "+mySistema.getNombre(),
                    Notification.Type.HUMANIZED_MESSAGE);
            notif.setDelayMsec(10000);
            notif.show(Page.getCurrent());
        });


        MenuBar.MenuItem bmClienteSis = bmSis.addItem("Cliente del Sistema", null, null);
        bmClienteSis.addItem("Agregar Cliente", e -> layoutH.addComponents(form));
        bmClienteSis.addItem("Mostrar Cliente", e -> {
            Cliente cli = mySistema.getClienteSistema();

            Notification notif = new Notification(
                    "Datos del Cliente",
                    "\n"
                            +"\nCui: "+ cli.getCui()
                            +"\nNombre: "+ cli.getNombre()
                            +"\nTelefono: "+ cli.getTelefono()
                            +"\nEdad: "+ cli.getEdad()
                            +"\nNit:"+cli.getNit(),
                    Notification.Type.HUMANIZED_MESSAGE);
            notif.setDelayMsec(10000);
            notif.show(Page.getCurrent());

        });

        MenuBar.MenuItem bmDesarrolladoresSis = bmSis.addItem("Desarrolladores del Sistema", null, null);
        bmDesarrolladoresSis.addItem("Agregar Desarrollador", e -> {
            formDesarrollador.setVisible(true);
            gridDes.setVisible(false);
            layoutH.addComponents(formDesarrollador);
        });
        bmDesarrolladoresSis.addItem("Mostrar Desarrolladores", e ->{
            Desarrollador[] listadoDes = mySistema.getListado_desarrolladores();
            gridDes.setVisible(true);
            gridDes.setItems(listadoDes);
            layoutH.addComponents(gridDes);

        });

        MenuBar.MenuItem bmRequerimientosSis = bmSis.addItem("Requerimientos del Sistema", null, null);
        bmRequerimientosSis.addItem("Agregar Requerimiento", e -> {
            gridRF.setVisible(false);
            gridNF.setVisible(false);
            formRequerimiento.setVisible(true);
            layoutH.addComponents(formRequerimiento);
        });
        bmRequerimientosSis.addItem("Mostrar Requerimientos Funcionales", e -> {
            gridNF.setVisible(false);
            gridRF.setVisible(true);
            //Grid Requerimientos Funcionales
            gridRF.setItems(mySistema.getRequerimientosFuncionales());
            layoutH.addComponents(gridRF);
        });
        bmRequerimientosSis.addItem("Mostrar Requerimientos No Funcionales", e -> {
            gridRF.setVisible(false);
            gridNF.setVisible(true);
            //Grid Requerimientos No Funcionales
            gridNF.setItems(mySistema.getRequerimientosNoFuncionales());
            layoutH.addComponents(gridNF);
        });

        //Datos Sistema
        btnAddSis.addClickListener(e ->{
            mySistema = new Sistema(Integer.parseInt(cantDes.getValue()));
            mySistema.setNombre(nomSis.getValue());

            Notification notif = new Notification(
                    "Datos de Sistemas",
                    "Ingresados Correctamente",
                    Notification.Type.HUMANIZED_MESSAGE);
            notif.setDelayMsec(10000);
            notif.show(Page.getCurrent());

            cantDes.setValue("");
            nomSis.setValue("");
            cantDes.setVisible(false);
            nomSis.setVisible(false);
            btnAddSis.setVisible(false);
        });



        gridRF.addColumn(Requerimiento::getDescripcion).setCaption("DESCRIPCION");
        gridNF.addColumn(Requerimiento::getDescripcion).setCaption("DESCRIPCION");
        //grid desarrolladores
        gridDes.addColumn(Desarrollador::getCui).setCaption("Cui");
        gridDes.addColumn(Desarrollador::getNombre).setCaption("Nombre");
        gridDes.addColumn(Desarrollador::getTelefono).setCaption("Teleono");
        gridDes.addColumn(Desarrollador::getEdad).setCaption("Edad");
        gridDes.addColumn(Desarrollador::getCodigo).setCaption("Codigo");
        gridDes.addColumn(Desarrollador::getSueldo).setCaption("Sueldo");
        gridDes.addColumn(Desarrollador::getProfesion).setCaption("Profesion");



        layoutV.addComponent(barmenu);

        layoutH.setMargin(true);
        layoutH.setSpacing(true);
        layoutH.setSizeFull();

        layoutH.addComponents(layoutV);
        setContent(layoutH);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
