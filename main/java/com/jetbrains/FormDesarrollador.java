package com.jetbrains;

import clases.Desarrollador;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import static com.vaadin.server.Page.getCurrent;

/**
 * Created by L99 on 7/13/2017.
 */
public class FormDesarrollador extends FormLayout {
    private TextField cui = new TextField("Cui");
    private TextField nombre = new TextField("Nombre");
    private TextField telefono = new TextField("Telefono");
    private TextField edad = new TextField("Edad");
    private TextField codigo = new TextField("Codigo");
    private TextField sueldo = new TextField("Sueldo");
    private TextField profesion = new TextField("Profesion");
    private Button btnSaveDes = new Button("Guardar Desarrollador");
    private Label title = new Label();


    private Desarrollador[] listado_desarrolladores;
    private MyUI myUI;

    public FormDesarrollador(MyUI myUI){
        this.myUI = myUI;
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(btnSaveDes);
        buttons.setSpacing(true);
        btnSaveDes.setStyleName(ValoTheme.BUTTON_PRIMARY);
        title.setValue("Ingresar Desarrollador");
        addComponent(title);
        addComponents(cui,nombre,telefono,edad,codigo,sueldo,profesion,buttons);
        btnSaveDes.addClickListener(e -> saveDesarrollador());
    }

    public void saveDesarrollador(){
//        Desarrollador desarrolladorSistema = new Desarrollador(Integer.parseInt(cui.getValue()), nombre.getValue(), telefono.getValue(), Integer.parseInt(edad.getValue()), Integer.parseInt(codigo.getValue()),Double.parseDouble(sueldo.getValue()),profesion.getValue());
        Desarrollador desarrolladorSistema = new Desarrollador();
        desarrolladorSistema.setCui(Integer.parseInt(cui.getValue()));
        desarrolladorSistema.setNombre(nombre.getValue());
        desarrolladorSistema.setTelefono(telefono.getValue());
        desarrolladorSistema.setEdad(Integer.parseInt(edad.getValue()));
        desarrolladorSistema.setCodigo(Integer.parseInt(codigo.getValue()));
        desarrolladorSistema.setSueldo(Double.parseDouble(sueldo.getValue()));
        desarrolladorSistema.setProfesion(profesion.getValue());

        try {
            myUI.mySistema.addDesarrollador(desarrolladorSistema);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        Notification notif = new Notification(
                "Datos del Desarrollador",
                desarrolladorSistema.getNombre()+" Ingresado Correctamente",
                Notification.Type.HUMANIZED_MESSAGE);
        notif.setDelayMsec(10000);
        notif.show(Page.getCurrent());

        cui.setValue("");
        nombre.setValue("");
        telefono.setValue("");
        edad.setValue("");
        codigo.setValue("");
        sueldo.setValue("");
        profesion.setValue("");
        title.setValue("");

        this.setVisible(false);
    }






}
