package com.jetbrains;

import clases.Cliente;
import clases.Requerimiento;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.Arrays;
import java.util.List;

import static com.vaadin.server.Page.getCurrent;

/**
 * Created by L99 on 7/13/2017.
 */
public class FormRequerimiento extends FormLayout {
    private TextArea descripcion = new TextArea("Descripción");
    private Button btnSaveReque = new Button("Guardar Requerimiento");
    private Label title = new Label();
    private List<String> data = Arrays.asList("Funcional","No Funcional");
    RadioButtonGroup sample = new RadioButtonGroup<>("Tipo de Requerimiento", data);


    private MyUI myUI;

    public FormRequerimiento(MyUI myUI){
        this.myUI = myUI;
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(btnSaveReque);
        buttons.setSpacing(true);
        btnSaveReque.setStyleName(ValoTheme.BUTTON_PRIMARY);
        title.setValue("Ingresar Requerimiento");
        sample.setValue("Funcional");
        addComponent(title);
        addComponents(descripcion, sample, buttons);
        btnSaveReque.addClickListener(e -> saveRequerimiento());
    }

    public void saveRequerimiento(){
        Requerimiento requerimientoF = new Requerimiento();
        Requerimiento requerimientoNF = new Requerimiento();

        if (sample.getValue() == "Funcional")
        {
            requerimientoF.setDescripcion(descripcion.getValue());
            myUI.mySistema.addRFuncionales(requerimientoF);
        }else
        {
            requerimientoNF.setDescripcion(descripcion.getValue());
            myUI.mySistema.addRNFuncionales(requerimientoNF);
        }


        Notification notif = new Notification(
                "Requerimiento",
                "Ingresado Correctamente",
                Notification.Type.WARNING_MESSAGE);
        notif.setDelayMsec(10000);
        notif.show(Page.getCurrent());
        descripcion.setValue("");
        this.setVisible(false);
    }






}
