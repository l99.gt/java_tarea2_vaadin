package clases;

/**
 * Created by L99 on 7/13/2017.
 */
public class Persona {
    private int cui;
    String nombre;
    String telefono;
    int edad;

    public Persona (){

    }

    //Constructor para iniciarlizar las variables.
    public Persona(int cui, String nombre, String telefono, int edad) {
        this.cui = cui;
        this.nombre = nombre;
        this.telefono = telefono;
        this.edad = edad;
    }

    //Metodos Get and Set para cada variable.
    public int getCui() {
        return cui;
    }

    public void setCui(int cui) {
        this.cui = cui;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}